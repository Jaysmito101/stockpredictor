cmake_minimum_required(VERSION 3.12)

set_property(GLOBAL PROPERTY USE_FOLDERS ON)

project(sand-box LANGUAGES C CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REUIRED True)

set(CMAKE_C_STANDARD 99)
set(CMAKE_C_STANDARD_REUIRED True)

find_package( OpenGL REQUIRED )
set(OPENSSL_USE_STATIC_LIBS TRUE)
find_package(OpenSSL REQUIRED)


include_directories( 
      ./lib/glfw/include
	./lib/glad/include
	./lib/csv/src
	./lib/imgui
	./lib/implot
	./lib/imgui/backends
	./lib/httplib
	./include
      ${OPENSSL_INCLUDE_DIR}
    )

set( GLFW_BUILD_DOCS OFF CACHE BOOL  "GLFW lib only" )
set( GLFW_INSTALL OFF CACHE BOOL  "GLFW lib only" )

add_subdirectory(lib/glfw)
add_subdirectory(lib/glad)

if( MSVC )
    SET( CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} /ENTRY:mainCRTStartup" )
endif()


file(GLOB SOURCES
include/*.hpp
include/*.h
source/*.cpp
)

list(APPEND SOURCES
	lib/imgui/imgui.cpp
	lib/imgui/imgui_demo.cpp
	lib/imgui/imgui_draw.cpp
	lib/imgui/imgui_tables.cpp
	lib/imgui/imgui_widgets.cpp
	lib/imgui/backends/imgui_impl_glfw.cpp
	lib/imgui/backends/imgui_impl_opengl3.cpp
	lib/implot/implot.cpp
	lib/implot/implot_demo.cpp
	lib/implot/implot_items.cpp
)



add_executable(StockPredict ${SOURCES})

target_link_libraries(StockPredict
glfw
${GLFW_LIBRARIES}
opengl32
glad
)

target_link_libraries(StockPredict OpenSSL::SSL)
target_link_libraries(StockPredict OpenSSL::Crypto)

if(MSVC)
    add_definitions(-D_CRT_SECURE_NO_WARNINGS)
endif()

if( MSVC )
    if(${CMAKE_VERSION} VERSION_LESS "3.6.0") 
        message( "\n\t[ WARNING ]\n\n\tCMake version lower than 3.6.\n\n\t - Please update CMake and rerun; OR\n\t - Manually set 'GLFW-CMake-starter' as StartUp Project in Visual Studio.\n" )
    else()
        set_property( DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} PROPERTY VS_STARTUP_PROJECT StockPredict)
    endif()
endif()