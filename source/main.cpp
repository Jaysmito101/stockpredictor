#define CGL_LOGGING_ENABLED
#define CGL_EXCLUDE_TEXT_RENDER
#define CGL_EXCLUDE_AUDIO
#define CGL_IMPLEMENTATION
#define CPPHTTPLIB_OPENSSL_SUPPORT
#include "cgl.h"
#include "httplib.h"
#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"
#include "implot.h"
#include "rapidcsv.h"

#include <filesystem>
#include <fstream>
#include <iostream>
#include <string>

static const std::string api_key = "##############";

static struct
{
	CGL_window* window; // window object
	CGL_framebuffer* framebuffer; // framebuffer object
	struct 
	{
		std::vector<std::string> time;
		std::vector<float> raw_close;
		std::vector<float> scaled_close;
		std::vector<float> ids;
		float close_min;
		float close_max;
		std::vector<float> predicted;
		std::vector<float> errors;
		bool loaded = false;
	} stocks_data;
	struct 
	{
		int id = 0;
		CGL_linear_regression_context* context = nullptr;
		float average_error = 0.0f;
		std::vector<float> errors;
		float each_batch_time = 0.0f;
		float total_time = 0.0f;
		float target_error = 0.0f;
	} model;
	int epoch = 1000;
	float train_ratio = 0.8f;
	float learning_rate = 0.01f;
	int max_months = 5;
} g_context; // global context



static void LoadDefaultStyle()
{
	ImGuiStyle *style = &ImGui::GetStyle();
	style->WindowPadding = ImVec2(15, 15);
	style->WindowRounding = 5.0f;
	style->FramePadding = ImVec2(5, 5);
	style->FrameRounding = 4.0f;
	style->ItemSpacing = ImVec2(12, 8);
	style->ItemInnerSpacing = ImVec2(8, 6);
	style->IndentSpacing = 25.0f;
	style->ScrollbarSize = 15.0f;
	style->ScrollbarRounding = 9.0f;
	style->GrabMinSize = 5.0f;
	style->GrabRounding = 3.0f;
	style->WindowBorderSize = 0;
	style->ChildBorderSize = 0;
	style->PopupBorderSize = 0;
	style->FrameBorderSize = 0;
	style->TabBorderSize = 0;
	style->Colors[ImGuiCol_Text] = ImVec4(0.80f, 0.80f, 0.83f, 1.00f);
	style->Colors[ImGuiCol_TextDisabled] = ImVec4(0.24f, 0.23f, 0.29f, 1.00f);
	style->Colors[ImGuiCol_WindowBg] = ImVec4(0.06f, 0.05f, 0.07f, 1.00f);
	style->Colors[ImGuiCol_PopupBg] = ImVec4(0.07f, 0.07f, 0.09f, 1.00f);
	style->Colors[ImGuiCol_Border] = ImVec4(0.80f, 0.80f, 0.83f, 0.88f);
	style->Colors[ImGuiCol_BorderShadow] = ImVec4(0.92f, 0.91f, 0.88f, 0.00f);
	style->Colors[ImGuiCol_FrameBg] = ImVec4(0.10f, 0.09f, 0.12f, 1.00f);
	style->Colors[ImGuiCol_FrameBgHovered] = ImVec4(0.24f, 0.23f, 0.29f, 1.00f);
	style->Colors[ImGuiCol_FrameBgActive] = ImVec4(0.56f, 0.56f, 0.58f, 1.00f);
	style->Colors[ImGuiCol_TitleBg] = ImVec4(0.10f, 0.09f, 0.12f, 1.00f);
	style->Colors[ImGuiCol_TitleBgCollapsed] = ImVec4(1.00f, 0.98f, 0.95f, 0.75f);
	style->Colors[ImGuiCol_TitleBgActive] = ImVec4(0.07f, 0.07f, 0.09f, 1.00f);
	style->Colors[ImGuiCol_MenuBarBg] = ImVec4(0.10f, 0.09f, 0.12f, 1.00f);
	style->Colors[ImGuiCol_ScrollbarBg] = ImVec4(0.10f, 0.09f, 0.12f, 1.00f);
	style->Colors[ImGuiCol_ScrollbarGrab] = ImVec4(0.80f, 0.80f, 0.83f, 0.31f);
	style->Colors[ImGuiCol_ScrollbarGrabHovered] = ImVec4(0.56f, 0.56f, 0.58f, 1.00f);
	style->Colors[ImGuiCol_ScrollbarGrabActive] = ImVec4(0.06f, 0.05f, 0.07f, 1.00f);
	style->Colors[ImGuiCol_CheckMark] = ImVec4(0.80f, 0.80f, 0.83f, 0.31f);
	style->Colors[ImGuiCol_SliderGrab] = ImVec4(0.80f, 0.80f, 0.83f, 0.31f);
	style->Colors[ImGuiCol_SliderGrabActive] = ImVec4(0.06f, 0.05f, 0.07f, 1.00f);
	style->Colors[ImGuiCol_Button] = ImVec4(0.10f, 0.09f, 0.12f, 1.00f);
	style->Colors[ImGuiCol_ButtonHovered] = ImVec4(0.24f, 0.23f, 0.29f, 1.00f);
	style->Colors[ImGuiCol_ButtonActive] = ImVec4(0.56f, 0.56f, 0.58f, 1.00f);
	style->Colors[ImGuiCol_Header] = ImVec4(0.10f, 0.09f, 0.12f, 1.00f);
	style->Colors[ImGuiCol_HeaderHovered] = ImVec4(0.56f, 0.56f, 0.58f, 1.00f);
	style->Colors[ImGuiCol_HeaderActive] = ImVec4(0.06f, 0.05f, 0.07f, 1.00f);
	style->Colors[ImGuiCol_ResizeGrip] = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
	style->Colors[ImGuiCol_ResizeGripHovered] = ImVec4(0.56f, 0.56f, 0.58f, 1.00f);
	style->Colors[ImGuiCol_ResizeGripActive] = ImVec4(0.06f, 0.05f, 0.07f, 1.00f);
	style->Colors[ImGuiCol_PlotLines] = ImVec4(0.40f, 0.39f, 0.38f, 0.63f);
	style->Colors[ImGuiCol_PlotLinesHovered] = ImVec4(0.25f, 1.00f, 0.00f, 1.00f);
	style->Colors[ImGuiCol_PlotHistogram] = ImVec4(0.40f, 0.39f, 0.38f, 0.63f);
	style->Colors[ImGuiCol_PlotHistogramHovered] = ImVec4(0.25f, 1.00f, 0.00f, 1.00f);
	style->Colors[ImGuiCol_TextSelectedBg] = ImVec4(0.25f, 1.00f, 0.00f, 0.43f);
	ImVec4 *colors = ImGui::GetStyle().Colors;
	colors[ImGuiCol_Tab] = ImVec4(0.146f, 0.113f, 0.146f, 0.86f);
	colors[ImGuiCol_TabHovered] = ImVec4(0.364f, 0.205f, 0.366f, 0.80f);
	colors[ImGuiCol_TabActive] = ImVec4(51.0f / 255, 31.0f / 255, 49.0f / 255, 0.97f);
	colors[ImGuiCol_TabUnfocused] = ImVec4(51.0f / 255, 31.0f / 255, 49.0f / 255, 0.57f);
	colors[ImGuiCol_Header] = ImVec4(0.61f, 0.61f, 0.62f, 0.22f);
	colors[ImGuiCol_HeaderHovered] = ImVec4(0.61f, 0.62f, 0.62f, 0.51f);
	colors[ImGuiCol_HeaderActive] = ImVec4(0.61f, 0.62f, 0.62f, 0.83f);
	colors[ImGuiCol_TabUnfocused] = ImVec4(43.0f/255, 17.0f/255, 43.0f/255, 0.97f);
	colors[ImGuiCol_TabUnfocusedActive] = ImVec4(0.202f, 0.116f, 0.196f, 0.57f);
}

static void initialize_imgui()
{
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO();
	io.IniFilename = "window_data.ini";
	io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
	io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;
	io.ConfigViewportsNoTaskBarIcon = false;
	io.ConfigViewportsNoAutoMerge = true;
	LoadDefaultStyle();
	ImGuiStyle& style = ImGui::GetStyle();
	ImGui_ImplGlfw_InitForOpenGL(CGL_window_get_glfw_handle(g_context.window), true);
	ImGui_ImplOpenGL3_Init("#version 430");
	ImPlot::CreateContext();
}

static void shutdown_imgui()
{
	ImPlot::DestroyContext();
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();
}

static void imgui_begin_docking()
{
	static bool dockspaceOpen = true;
	static bool opt_fullscreen_persistant = true;
	bool opt_fullscreen = opt_fullscreen_persistant;
	static ImGuiDockNodeFlags dockspace_flags = ImGuiDockNodeFlags_None;
	ImGuiWindowFlags window_flags = ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoDocking;

	if (opt_fullscreen)
	{
		ImGuiViewport *viewport = ImGui::GetMainViewport();
		ImGui::SetNextWindowPos(viewport->Pos);
		ImGui::SetNextWindowSize(viewport->Size);
		ImGui::SetNextWindowViewport(viewport->ID);
		ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.1f);
		ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
		window_flags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove;
		window_flags |= ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus;
	}

	if (dockspace_flags & ImGuiDockNodeFlags_PassthruCentralNode)
	{
		window_flags |= ImGuiWindowFlags_NoBackground;
	}

	ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
	ImGui::Begin("DockSpace", &dockspaceOpen, window_flags);
	ImGui::PopStyleVar();

	if (opt_fullscreen)
	{
		ImGui::PopStyleVar(2);
	}

	// DockSpace
	ImGuiIO &io = ImGui::GetIO();
	ImGuiStyle &style = ImGui::GetStyle();
	float minWinSizeX = style.WindowMinSize.x;
	style.WindowMinSize.x = 370.0f;

	if (io.ConfigFlags & ImGuiConfigFlags_DockingEnable)
	{
		ImGuiID dockspace_id = ImGui::GetID("MyDockSpace");
		ImGui::DockSpace(dockspace_id, ImVec2(0.0f, 0.0f), dockspace_flags);
	}

	style.WindowMinSize.x = minWinSizeX;
}

static void imgui_new_frame()
{
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplGlfw_NewFrame();
	ImGui::NewFrame();
	imgui_begin_docking();
}

static void imgui_end_frame()
{
	ImGui::End();
	ImGui::EndFrame();
	ImGui::Render();
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}

static void download_file(const std::string& baseURL, const std::string& urlPath, const std::string& path)
{
	try
	{
		httplib::Client cli(baseURL);
		FILE* file = fopen(path.c_str(), "wb");
		if (file == NULL) throw std::runtime_error("Could not open file for writing");
		auto res = cli.Get(urlPath.c_str(), [&](const char* data, size_t data_length) -> bool
		{
			fwrite(data, 1, data_length, file);
			return true;
		});
		fclose(file);
	}
	catch(const std::exception& e)
	{
		std::cerr << e.what() << '\n';
	}
}

static void load_stock_data(const std::string& stock_name)
{
	// load stock history data from name using an api
	// format https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY_EXTENDED&symbol=IBM&interval=15min&slice=year1month1&apikey=demo
	const std::string baseURL = "https://www.alphavantage.co";
	for (int i = 0 ; i < g_context.max_months; i++)
	{
		const std::string path = "stock_data_" + stock_name + "_" + std::to_string(i) + ".csv";
		if (std::filesystem::exists(path)) continue;
		const std::string urlPath = "/query?function=TIME_SERIES_INTRADAY_EXTENDED&symbol=" + stock_name + "&interval=30min&apikey=" + api_key + "&slice=year1month" + std::to_string(i + 1);
		download_file(baseURL, urlPath, path);
	}

	g_context.stocks_data.loaded = false;
	g_context.stocks_data.time.clear();
	g_context.stocks_data.raw_close.clear();
	g_context.stocks_data.scaled_close.clear();
	g_context.stocks_data.ids.clear();
	g_context.stocks_data.predicted.clear();
	g_context.model.errors.clear();


	// load stock history data from the downloaded files
	for (int i = 0 ; i < g_context.max_months; i++)
	{
		const std::string path = "stock_data_" + stock_name + "_" + std::to_string(i) + ".csv";
		rapidcsv::Document doc(path);
		auto time_d = doc.GetColumn<std::string>("time"); g_context.stocks_data.time.insert(g_context.stocks_data.time.end(), time_d.begin(), time_d.end());
		auto close_d = doc.GetColumn<double>("close"); g_context.stocks_data.raw_close.insert(g_context.stocks_data.raw_close.end(), close_d.begin(), close_d.end());
	}

	auto close = std::minmax_element(g_context.stocks_data.raw_close.begin(), g_context.stocks_data.raw_close.end());

	g_context.stocks_data.close_min = *close.first;
	g_context.stocks_data.close_max = *close.second;

	for (int i = 0 ; i < g_context.stocks_data.time.size() ; i++)
	{
		g_context.stocks_data.scaled_close.push_back((g_context.stocks_data.raw_close[i] - *close.first) / (*close.second - *close.first));
		g_context.stocks_data.ids.push_back((float)i / (float)g_context.stocks_data.time.size());
	}

	g_context.stocks_data.loaded = true;
}

static void create_model(int type)
{
	if(g_context.model.context) CGL_linear_regression_context_destroy(g_context.model.context);
	g_context.model.id = 0;
	g_context.model.context = nullptr;
	g_context.model.errors.clear();
	g_context.model.total_time = 0.0f;
	
	switch (type)
	{
	case 1: g_context.model.context = CGL_linear_regression_context_create(1); break;
	case 2: g_context.model.context = CGL_linear_regression_context_create(10); break;
	case 3: g_context.model.context = CGL_linear_regression_context_create(5); break;
	default: return;
	}
	CGL_linear_regression_randomize_coefficents(g_context.model.context, -0.5f, 0.5f);
	g_context.model.id = type;
}

CGL_float sample_function0(CGL_void* user_data, CGL_float* input, CGL_float* output, CGL_int id)
{
	*input = g_context.stocks_data.ids[id]; // set the x value of the point
    return g_context.stocks_data.scaled_close[id]; // return the y value of the point
}

CGL_float sample_function1(CGL_void* user_data, CGL_float* input, CGL_float* output, CGL_int id)
{
	for (int i = 0 ; i < 10 ; i++)
	{
		*(input + i) = g_context.stocks_data.scaled_close[id + i];
	}
	return g_context.stocks_data.scaled_close[id + 10]; // return the y value of the point
}

CGL_float sample_function2(CGL_void* user_data, CGL_float* input, CGL_float* output, CGL_int id)
{
	// polynomial regression with id as x
	float x = g_context.stocks_data.ids[id];
	// give 5 different powers of x as input
	*input = x;
	*(input + 1) = x * x;
	*(input + 2) = x * x * x;
	*(input + 3) = x * x * x * x;
	*(input + 4) = x * x * x * x * x;
	return g_context.stocks_data.scaled_close[id]; // return the y value of the point
}

void predict(int model);

static void train_model()
{
	g_context.model.each_batch_time = 0.0f;
	auto start = std::chrono::high_resolution_clock::now();
	switch (g_context.model.id)
	{
	case 1: CGL_linear_regression_train(g_context.model.context, sample_function0, NULL, (int)(g_context.stocks_data.ids.size() * g_context.train_ratio), g_context.learning_rate, g_context.epoch); break;
	case 2: CGL_linear_regression_train(g_context.model.context, sample_function1, NULL, (int)(g_context.stocks_data.ids.size() * g_context.train_ratio) - 10, g_context.learning_rate, g_context.epoch); break;
	case 3: CGL_linear_regression_train(g_context.model.context, sample_function2, NULL, (int)(g_context.stocks_data.ids.size() * g_context.train_ratio), g_context.learning_rate, g_context.epoch); break;
	default: return;
	}
	auto end = std::chrono::high_resolution_clock::now();
	g_context.model.each_batch_time = (float)std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
	g_context.model.total_time += g_context.model.each_batch_time;
	predict(g_context.model.id);
	g_context.model.errors.push_back(g_context.model.average_error);
}

void predict(int model)
{
	int count = (int)(g_context.stocks_data.ids.size());

	g_context.stocks_data.predicted.clear();
	g_context.stocks_data.errors.clear();
	g_context.model.average_error = 0.0f;


	if(model == 1)
	{
		for (int i = 0; i < count; i++)
		{
			CGL_float input = g_context.stocks_data.ids[i];
			CGL_float output = 0;
			CGL_linear_regression_evaluate(g_context.model.context, &input, &output);
			if(i < count + 100)
			{
				g_context.stocks_data.errors.push_back((output - g_context.stocks_data.scaled_close[i]) * (g_context.stocks_data.close_max - g_context.stocks_data.close_min));
				g_context.model.average_error += powf(output - g_context.stocks_data.scaled_close[i], 2.0f);
			}
			g_context.stocks_data.predicted.push_back(output * (g_context.stocks_data.close_max - g_context.stocks_data.close_min) + g_context.stocks_data.close_min);		
		}
		return;
	}

	if (model == 2)
	{
		std::vector<float> scaled_predicted;
		for (int i = 0 ; i < 10; i++) { g_context.stocks_data.predicted.push_back(g_context.stocks_data.raw_close[i]); scaled_predicted.push_back(g_context.stocks_data.scaled_close[i]); }
		for (int i = 0; i < count; i++)
		{
			CGL_float input[10];
			for (int j = 0; j < 10; j++) input[j] = scaled_predicted[i + j];
			CGL_float output = 0;
			CGL_linear_regression_evaluate(g_context.model.context, input, &output);
			if(i < count - 10)
			{
				g_context.stocks_data.errors.push_back((output - g_context.stocks_data.scaled_close[i + 10]) * (g_context.stocks_data.close_max - g_context.stocks_data.close_min));
				g_context.model.average_error += powf(output - g_context.stocks_data.scaled_close[i + 10], 2.0f);
			}
			scaled_predicted.push_back(output);
			g_context.stocks_data.predicted.push_back(output * (g_context.stocks_data.close_max - g_context.stocks_data.close_min) + g_context.stocks_data.close_min);		
		}
	}

	if(model == 3)
	{
		for (int i = 0; i < count; i++)
		{
			CGL_float x = g_context.stocks_data.ids[i];
			CGL_float input[5] = {x, x * x, x * x * x, x * x * x * x, x * x * x * x * x};
			CGL_float output = 0;
			CGL_linear_regression_evaluate(g_context.model.context, input, &output);
			if(i < count)
			{
				g_context.stocks_data.errors.push_back((output - g_context.stocks_data.scaled_close[i]) * (g_context.stocks_data.close_max - g_context.stocks_data.close_min));
				g_context.model.average_error += powf(output - g_context.stocks_data.scaled_close[i], 2.0f);
			}
			g_context.stocks_data.predicted.push_back(output * (g_context.stocks_data.close_max - g_context.stocks_data.close_min) + g_context.stocks_data.close_min);		
		}
		return;
	}

	g_context.model.average_error = sqrtf( g_context.model.average_error / (float)count );

}

static void show_regression_window()
{
	ImGui::Begin("Regression Settings");

	static char stock_name_buffer[256] = "IBM";
	ImGui::InputTextWithHint("##stock_name", "Enter stock name", stock_name_buffer, 256);
	ImGui::SameLine();
	if (ImGui::Button("Load Stock")) { load_stock_data(stock_name_buffer); }
	ImGui::InputInt("Max Months Data##max_months", &g_context.max_months);

	static int model = 0;
	static const char* model_names[] = { "None", "Linear Regression", "Multivariate Regression", "Polynomial Regression (Power 5)"};
	// show the model selection combo box
	if (ImGui::BeginCombo("Model", model_names[model]))
	{
		for (int i = 0; i < IM_ARRAYSIZE(model_names); i++)
		{
			bool is_selected = (model == i);
			if (ImGui::Selectable(model_names[i], is_selected)) model = i;
			if (is_selected) ImGui::SetItemDefaultFocus();
		}
		ImGui::EndCombo();
	}

	if (g_context.model.id != model) create_model(model);

	ImGui::InputFloat("Target RMS Error##error", &g_context.model.target_error);
	ImGui::InputInt("Batch Epochs##epochs", &g_context.epoch);
	ImGui::SliderFloat("Training Ratio##training_ratio", &g_context.train_ratio, 0.1f, 1.0f);
	ImGui::SliderFloat("Learning Rate##learning_rate", &g_context.learning_rate, 0.0f, 1.0f);

	static float random_range[2] = { -0.5f, 0.5f };
	ImGui::SliderFloat2("Random Range##random_range", random_range, -2.0f, 2.0f);
	if (ImGui::Button("Randomize Weights")) if(model != 0) { CGL_linear_regression_randomize_coefficents(g_context.model.context, random_range[0], random_range[1]); g_context.model.errors.clear(); g_context.model.total_time = 0.0;}
	ImGui::SameLine();
	if (ImGui::Button("Train Model")) train_model(); 
	ImGui::SameLine();
	if (ImGui::Button("Predict")) predict(model);

	static bool auto_train = false;
	ImGui::Checkbox("Auto Train", &auto_train);
	if (auto_train)
	{
		if (g_context.model.average_error <= g_context.model.target_error) auto_train = false;
		else train_model();
	}

	ImGui::Separator();
	ImGui::Text("RMS Error: %f", g_context.model.average_error);
	ImGui::Text("Batch Training Time: %f", g_context.model.each_batch_time);
	ImGui::Text("Total Training Time: %f", g_context.model.total_time);


	ImGui::End();

	ImGui::Begin("Stock Price Visualization");

	if (ImPlot::BeginPlot("Stock Prices")) 
	{
        ImPlot::SetupAxes("Hours", "Price");
        ImPlot::SetupAxesLimits(0,100, 120.0, 130.0);
        if(g_context.stocks_data.loaded) ImPlot::PlotLine("Actual", g_context.stocks_data.raw_close.data(), (int)g_context.stocks_data.raw_close.size());
        if(g_context.stocks_data.predicted.size() > 0) ImPlot::PlotLine("Predicted", g_context.stocks_data.predicted.data(), (int)g_context.stocks_data.predicted.size());
        ImPlot::EndPlot();
    }

	if (ImPlot::BeginPlot("Root Mean Square Error (vs Number of Batches Trained)")) 
	{
        ImPlot::SetupAxes("Training Batches", "RMS Error");
        ImPlot::SetupAxesLimits(0, 100, 0.0, 10.0);
        //if(g_context.stocks_data.loaded) ImPlot::PlotLine("Actual", g_context.stocks_data.raw_close.data(), (int)g_context.stocks_data.raw_close.size());
        if(g_context.model.errors.size() > 0) ImPlot::PlotLine("Errors", g_context.model.errors.data(), (int)g_context.model.errors.size());
        ImPlot::EndPlot();
    }
	

	if (ImPlot::BeginPlot("Errors")) 
	{
        ImPlot::SetupAxes("Hours", "Error");
        ImPlot::SetupAxesLimits(0,100, -50.0, 50.0);
        //if(g_context.stocks_data.loaded) ImPlot::PlotLine("Actual", g_context.stocks_data.raw_close.data(), (int)g_context.stocks_data.raw_close.size());
        if(g_context.stocks_data.errors.size() > 0) ImPlot::PlotLine("Errors", g_context.stocks_data.errors.data(), (int)g_context.stocks_data.errors.size());
        ImPlot::EndPlot();
    }



	ImGui::End();
}

CGL_int main()
{
	srand((uint32_t)time(NULL)); // seed the random number generator
	if(!CGL_init()) return EXIT_FAILURE; // initialize CGL (required for setting up internals of CGL)
	g_context.window = CGL_window_create(700, 700, "Stock Price Predictor - Jaysmito Mukherjee"); // create the window
	CGL_window_make_context_current(g_context.window); // make the opengl context for the window current
	if(!CGL_gl_init()) return EXIT_FAILURE; // initialize cgl opengl module    
	g_context.framebuffer = CGL_framebuffer_create_from_default(g_context.window); // load the default framebuffer (0) into CGL_framebuffer object
	CGL_widgets_init(); // initialize the widgets module
	initialize_imgui(); // initialize imgui
	CGL_float curr_time = CGL_utils_get_time(); // get the current time
	CGL_int frame_count = 0, current_player_sprite = 0, closest_pipe_index = 0, score = 0, last_scored_pipe = -1; // initialize the frame count, current player sprite, closest pipe index, score and last scored pipe index
	while(!CGL_window_should_close(g_context.window)) // run till the close button is clicked
	{
			frame_count++; curr_time = CGL_utils_get_time();
			CGL_framebuffer_bind(g_context.framebuffer); // bind default framebuffer and also adjust viewport size and offset
			CGL_gl_clear(0.0f, 0.0f, 0.0f, 1.0f); // clear screen with a dark gray color        

			imgui_new_frame();

			show_regression_window();

			imgui_end_frame();		

			if(CGL_window_is_key_pressed(g_context.window, CGL_KEY_ESCAPE)) break;
			CGL_window_swap_buffers(g_context.window); // swap framebuffers
			CGL_window_poll_events(g_context.window); // poll events (if this is not called every frame window will stop responding)
	}
	shutdown_imgui(); // shutdown imgui
	CGL_widgets_shutdown();
	CGL_framebuffer_destroy(g_context.framebuffer); // destory framebuffer object
	CGL_gl_shutdown(); // shutdown cgl opengl module
	CGL_window_destroy(g_context.window); // destroy window
	CGL_shutdown(); // shutdown cgl and clean up resources allocated by CGL internally (if any)
	return EXIT_SUCCESS;
}